export const DATA_LOADED = "DATA_LOADED";
export const NEXT_QUESTION = "NEXT_QUESTION";
export const START_GAME = "START_GAME";
export const PLAY_AGAIN = "PLAY_AGAIN";
