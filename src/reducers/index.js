import {
  DATA_LOADED,
  NEXT_QUESTION,
  START_GAME,
  PLAY_AGAIN
} from "../constants/action-types";
import {
  APP_STATE_INSTRUCTIONS,
  APP_STATE_QUESTIONS,
  APP_STATE_RESULTS
} from "../constants/app-states";
//import produce from "immer";
const initialState = {
  articles: [],
  remoteQuestions: [],
  currentQuestion: 0,
  appState: APP_STATE_INSTRUCTIONS
};

function rootReducer(state = initialState, action) {
  if (action.type === START_GAME) {
    return Object.assign({}, state, {
      appState: APP_STATE_QUESTIONS
    });
  }

  if (action.type === DATA_LOADED) {
    return Object.assign({}, state, {
      remoteQuestions: state.remoteQuestions.concat(action.payload)
    });
  }

  if (action.type === NEXT_QUESTION) {
    let answeredCorrectly =
      state.remoteQuestions[state.currentQuestion].correct_answer ===
      action.payload;

    const nState = Object.assign({}, state, {
      remoteQuestions: state.remoteQuestions.map((item, index) => {
        return index !== state.currentQuestion
          ? item
          : {
              ...item,
              answered: action.payload,
              answeredCorrectly: answeredCorrectly
            };
      })
    });

    if (state.remoteQuestions.length > state.currentQuestion + 1) {
      return Object.assign({}, nState, {
        currentQuestion: nState.currentQuestion + 1
      });
    } else {
      return Object.assign({}, nState, {
        appState: APP_STATE_RESULTS
      });
    }
  }

  if (action.type === PLAY_AGAIN) {
    return Object.assign({}, state, initialState);
  }

  return state;
}

export default rootReducer;
