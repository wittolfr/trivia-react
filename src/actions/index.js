import {
  DATA_LOADED,
  NEXT_QUESTION,
  START_GAME,
  PLAY_AGAIN
} from "../constants/action-types";
import { API_URL } from "../constants/app-config";

export function nextQuestion(payload) {
  return {
    type: NEXT_QUESTION,
    payload
  };
}

export function startGame(payload) {
  return {
    type: START_GAME,
    payload
  };
}

export function playAgain(payload) {
  return {
    type: PLAY_AGAIN,
    payload
  };
}

export function getData() {
  return function(dispatch) {
    return fetch(API_URL)
      .then(response => response.json())
      .then(json => {
        dispatch({ type: DATA_LOADED, payload: json.results });
      });
  };
}
