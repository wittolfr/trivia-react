import React, { Component } from "react";
import { connect } from "react-redux";
import { nextQuestion } from "../actions/index";
import QuestionsCounter from "./QuestionsCounter";

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

function mapDispatchToProps(dispatch) {
  return {
    nextQuestion: question => dispatch(nextQuestion(question))
  };
}

function mapStateToProps(state) {
  return {
    articles: state.remoteQuestions.slice(0, 10),
    currentQuestion: state.currentQuestion
  };
}

class ConnectedQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: false,
      question: {}
    };
    this.handleChangeAnswerChange = this.handleChangeAnswerChange.bind(this);
  }

  handleChangeAnswerChange(event) {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });
    this.props.nextQuestion(event.target.value);
    this.setState({ answer: false });
  }

  render() {
    const question = this.props.articles[this.props.currentQuestion];
    return (
      <div>
        <Typography variant="h3" component="h3" style={{ minHeight: "17vh" }}>
          <b>{question && question.category}</b>
        </Typography>

        <Card
          style={{
            display: "flex",
            alignItems: "center",
            padding: "1vh",
            minHeight: "40vh"
          }}
        >
          <CardContent>
            <Typography variant="h4" component="h4" color="textSecondary">
              {question && (
                <span
                  dangerouslySetInnerHTML={{ __html: question.question }}
                ></span>
              )}
            </Typography>
          </CardContent>
        </Card>

        <Typography variant="h5" component="h5" color="textSecondary">
          Answer
        </Typography>
        <RadioGroup
          row
          style={{ justifyContent: "center" }}
          aria-label="gender"
          name="answer"
          id="answer"
          value={this.state.answer}
          onChange={this.handleChangeAnswerChange}
        >
          {question &&
            [...question.incorrect_answers, question.correct_answer]
              .sort(function(a, b) {
                return 0.5 - Math.random();
              })
              .map((answer, index) => (
                <FormControlLabel
                  key={index}
                  value={answer}
                  control={<Radio />}
                  label={answer}
                />
              ))}
        </RadioGroup>

        <QuestionsCounter />
      </div>
    );
  }
}
const Question = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedQuestion);
export default Question;
