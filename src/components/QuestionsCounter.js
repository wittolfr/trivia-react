import React, { Component } from "react";
import { connect } from "react-redux";

function mapDispatchToProps(dispatch) {
  return {
  };
}

function mapStateToProps(state) {
  return {
    articles: state.remoteQuestions.slice(0, 10),
    currentQuestion: state.currentQuestion
  };
}

class ConnectedQuestionCounter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: null,
      question: {}
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.id]: event.target.value });
  }

  handleSubmit(event) {
    const { title } = this.state;
    this.props.addArticle({ title, id: title });
  }
  render() {
    return (
      <h6>
        {this.props.currentQuestion + 1} of {this.props.articles.length}
      </h6>
    );
  }
}
const QuestionCounter = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedQuestionCounter);
export default QuestionCounter;
