import React from "react";
import { connect } from "react-redux";
//import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Instructions from "./Instructions";
import Question from "./Question";
import Results from "./Results";

import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Paper from "@material-ui/core/Paper";

function mapStateToProps(state) {
  return {
    appState: state.appState
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const ConnectedApp = props => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" >
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" style={{margin:"0 auto"}}>TRIVIA</Typography>
          </Toolbar>
        </AppBar>
        <Paper className={classes.root} style={{textAlign: "center"}}>
          {
            //Simpler navigation  than react-router using state-switch and POJO
            {
              APP_STATE_INSTRUCTIONS: <Instructions />,
              APP_STATE_QUESTIONS: <Question />,
              APP_STATE_RESULTS: <Results />
            }[props.appState]
          }
        </Paper>
      </Container>
    </React.Fragment>
  );
};

const App = connect(
  mapStateToProps
  //mapDispatchToProps
)(ConnectedApp);
export default App;
