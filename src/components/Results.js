// src/js/components/Form.js
import React, { Component } from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

/* icons are failing not included in this version
import { FaRegFlushed, FaRegGrin } from 'react-icons/fa';
{(el.correct_answer === el.answered)?<FaRegGrin />:<FaRegFlushed/>}
*/

import { playAgain } from "../actions/index";

function mapDispatchToProps(dispatch) {
  return {
    playAgain: () => dispatch(playAgain())
  };
}

function mapStateToProps(state) {
  return {
    questions: state.remoteQuestions.slice(0, 10)
  };
}

class ConnectedResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: null,
      question: {}
    };
    this.handlereStartGame = this.handlereStartGame.bind(this);
  }

  handlereStartGame() {
    this.props.playAgain();
  }
  render() {
    return (
      <div>
        <Typography variant="h3" component="h3">
          You Scored <br />
          {this.props.questions.reduce((acc, q) => {
            return acc + (q.correct_answer === q.answered ? 1 : 0);
          }, 0)} / {this.props.questions.length}
        </Typography>

        <ul>
          {this.props.questions.map(el => (
            <li key={el.question} style={{ margin: "3vh 4vw" }}>
              <Typography variant="caption" component="div">
                <h2 style={{ display: "inline" }}>
                  {el.correct_answer === el.answered ? "+ " : "- "}
                </h2>
                <span dangerouslySetInnerHTML={{ __html: el.question }}></span>
                <br />
                <Typography
                  color="textSecondary"
                  variant="caption"
                  component="p"
                >
                  {" Right Answer: " + el.correct_answer + ""}
                  {" Your answer: " + el.answered + ""}
                </Typography>
              </Typography>
              <Divider />
            </li>
          ))}
        </ul>

        <Button
          color="primary"
          variant="contained"
          onClick={this.handlereStartGame}
          style={{ marginTop: "15vh" }}
        >
          PLAY AGAIN?
        </Button>
      </div>
    );
  }
}
const Results = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedResults);
export default Results;
