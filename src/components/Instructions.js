// src/js/components/Form.js
import React, { Component } from "react";
import { connect } from "react-redux";
import { getData } from "../actions/index";
import { startGame } from "../actions/index";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

function mapDispatchToProps(dispatch) {
  return {
    startGame: () => dispatch(startGame()),
    getData: () => dispatch(getData())
  };
}

function mapStateToProps(state) {
  return {};
}

class ConnectedInstructions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };

    this.handleStartGame = this.handleStartGame.bind(this);
  }

  componentDidMount() {
    this.props.getData();
  }

  handleStartGame() {
    this.props.startGame();
  }

  render() {
    return (
      <div>
        <Typography variant="h3" component="h3">
          <b>
            Welcome to the <br />
            Trivia Challenge!
          </b>
        </Typography>

        <Typography variant="h3" component="h3" style={{ marginTop: "15vh" }}>
          You will be presented with 10 True or False questions.
        </Typography>

        <Typography variant="h3" component="h3" style={{ marginTop: "15vh" }}>
          Can you score 100%?
        </Typography>

        <Button
          color="primary"
          variant="contained"
          onClick={this.handleStartGame}
          style={{ marginTop: "15vh" }}
        >
          BEGIN
        </Button>
      </div>
    );
  }
}
const Instructions = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedInstructions);
export default Instructions;
