## Overview

This is a trivia game yes/no questions challenge created  with:
> React, Redux, Thunk, material-ui

> @autor wittolfr@yahoo.ca 

It load the questions from
[https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean](https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean)

The flow of the trivia is handled by a state variable called appState
than mutable between this values:

* APP_STATE_INSTRUCTIONS 

   Here instructions are given to the user and data is loading using react-thunk  and stored in redux.  
* APP_STATE_QUESTIONS 

   Here questions and posible answers are displayed, the actual answer is stored alogn the original question to display totals later.  
* APP_STATE_RESULTS 

   Here results are displayed from the store and score is shown.



## Demo


## Installation
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
just run 
### `yarn install`


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


